package com.rezaee.rassetcollector.propertystatus;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.orm.SugarRecord;
import com.rezaee.rassetcollector.R;
import com.rezaee.rassetcollector.core.MainActivity;
import com.rezaee.rassetcollector.model.Property;
import com.rezaee.rassetcollector.model.PropertyStatus;

import java.util.ArrayList;
import java.util.List;

public class PropertyStatusRecyclerAdapter extends RecyclerView.Adapter<PropertyStatusRecyclerAdapter.PropertyStatusViewHolder> implements Filterable {
	private Context context;
	private List<PropertyStatus> resource;
	private List<PropertyStatus> filteredResource;
	private LayoutInflater layoutInflater;

	public PropertyStatusRecyclerAdapter(Context context, List<PropertyStatus> propertyStatuses) {
		this.context = context;
		this.resource = propertyStatuses;
		this.filteredResource = propertyStatuses;
		this.layoutInflater = LayoutInflater.from(context);
	}

	@Override
	public PropertyStatusViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = layoutInflater.inflate(R.layout.item_property_status, parent, false);

		return new PropertyStatusViewHolder(view);
	}

	@Override
	public void onBindViewHolder(PropertyStatusViewHolder holder, int position) {
		holder.SetData(filteredResource.get(position));
	}

	@Override
	public int getItemCount() {
		return filteredResource.size();
	}

	public void deleteItem(final PropertyStatus item, final int currentPosition) {
		new AlertDialog.Builder(context)
			.setTitle(R.string.Caution)
			.setMessage(R.string.AreYouSure)
			.setIcon(R.drawable.ic_caution)
			.setPositiveButton(R.string.Ok, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					if (validateDelete(item)) {
						PropertyStatus propertyStatus = PropertyStatus.findById(PropertyStatus.class, item.getId());
						propertyStatus.delete();

						filteredResource = SugarRecord.listAll(PropertyStatus.class);

						notifyItemRemoved(currentPosition);
					}

					notifyItemRangeChanged(currentPosition, filteredResource.size());
				}
			})
			.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					notifyItemRangeChanged(currentPosition, filteredResource.size());
				}
			})
			.show();
	}

	private boolean validateDelete(PropertyStatus propertyStatus) {
		long resultCount = Property.count(Property.class, "property_status_id = ?", new String[]{String.valueOf(propertyStatus.getId())});

		if (resultCount > 0) {
			Toast.makeText(context, R.string.ItemIsInUse, Toast.LENGTH_LONG).show();

			return false;
		}

		return true;
	}

	public void editItem(PropertyStatus propertyStatus) {
		PropertyStatusDefineFragment propertyStatusDefineFragment = new PropertyStatusDefineFragment();

		propertyStatusDefineFragment.SetData(propertyStatus);

		FragmentTransaction fragmentTransaction = ((MainActivity) context).getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.frlMainContainer, propertyStatusDefineFragment);
		fragmentTransaction.commit();
	}

	@Override
	public Filter getFilter() {
		return new Filter() {
			@Override
			protected FilterResults performFiltering(CharSequence charSequence) {
				FilterResults results = new FilterResults();

				charSequence = charSequence.toString().toLowerCase();

				if (charSequence.length() == 0) {
					results.values = resource;
					results.count = resource.size();
				} else {
					List<PropertyStatus> filterResultsData = new ArrayList<>();

					for (PropertyStatus data : resource) {
						String title = data.getTitle();

						if (title.contains(charSequence))
							filterResultsData.add(data);
					}

					results.values = filterResultsData;
					results.count = filterResultsData.size();
				}

				return results;
			}

			@Override
			protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
				filteredResource = (ArrayList<PropertyStatus>) filterResults.values;

				notifyDataSetChanged();
			}
		};
	}

	class PropertyStatusViewHolder extends RecyclerView.ViewHolder {
		public PropertyStatus currentObject;

		private TextView lblTitle;

		public PropertyStatusViewHolder(View itemView) {
			super(itemView);

			lblTitle = (TextView) itemView.findViewById(R.id.lblTitle);

			LinearLayout cmdEdit = (LinearLayout) itemView.findViewById(R.id.cmdEdit);
			LinearLayout cmdDelete = (LinearLayout) itemView.findViewById(R.id.cmdDelete);

			cmdEdit.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					editItem(currentObject);
				}
			});

			cmdDelete.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					deleteItem(currentObject, getAdapterPosition());
				}
			});
		}

		public void SetData(PropertyStatus currentObject) {
			this.currentObject = currentObject;

			lblTitle.setText(currentObject.getTitle());
		}
	}
}