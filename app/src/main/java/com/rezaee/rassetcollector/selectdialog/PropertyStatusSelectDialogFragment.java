package com.rezaee.rassetcollector.selectdialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.rezaee.rassetcollector.Property.PropertyDefineFragment;
import com.rezaee.rassetcollector.R;
import com.rezaee.rassetcollector.model.PropertyStatus;

import java.util.List;

public class PropertyStatusSelectDialogFragment extends DialogFragment {
	@NonNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		final List<PropertyStatus> propertyStatuses = PropertyStatus.find(PropertyStatus.class, "active = ?", "1");

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater layoutInflater = getActivity().getLayoutInflater();
		View view = layoutInflater.inflate(R.layout.fragment_select_view_list, null);

		final ListView baseListView = (ListView) view.findViewById(R.id.lvuBaseSelectListView);
		final CustomArrayAdapter<PropertyStatus> adapter = new CustomArrayAdapter<>(getContext(), propertyStatuses);
		baseListView.setAdapter(adapter);

		baseListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				if (getTargetFragment() instanceof PropertyDefineFragment) {
					((PropertyDefineFragment) getTargetFragment()).SetPropertyStatus(propertyStatuses.get(i));
				}

				dismiss();
			}
		});

		EditText txtSearch = (EditText) view.findViewById(R.id.txtSearch);

		txtSearch.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
			}

			@Override
			public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
			}

			@Override
			public void afterTextChanged(Editable editable) {
				adapter.getFilter().filter(editable);
			}
		});

		builder
			.setView(view)
			.setTitle(R.string.PropertyStatus)
			.setIcon(R.drawable.ic_property_status)
			.setNegativeButton(R.string.Cancel, null);

		return builder.create();
	}
}