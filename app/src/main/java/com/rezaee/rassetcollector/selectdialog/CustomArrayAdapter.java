package com.rezaee.rassetcollector.selectdialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.rezaee.rassetcollector.R;
import com.rezaee.rassetcollector.model.IFilterableClass;

import java.util.ArrayList;
import java.util.List;

public class CustomArrayAdapter<T extends IFilterableClass> extends ArrayAdapter<T> implements Filterable {
	List<T> resource;
	List<T> filteredResource;

	public CustomArrayAdapter(Context context, int resource, List<T> objects) {
		super(context, resource, objects);

		this.resource = objects;
		this.filteredResource = objects;
	}

	public CustomArrayAdapter(Context context, List<T> objects) {
		super(context, R.layout.item_simple_view, objects);

		this.resource = objects;
		this.filteredResource = objects;
	}

	@NonNull
	@Override
	public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
		T data = getItem(position);

		LayoutInflater layoutInflater = LayoutInflater.from(getContext());
		View itemView = layoutInflater.inflate(R.layout.item_simple_view, parent, false);

		TextView txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
		txtTitle.setText(data.getTitle());

		return itemView;
	}

	@Override
	public int getCount() {
		return filteredResource.size();
	}

	@Nullable
	@Override
	public T getItem(int position) {
		return filteredResource.get(position);
	}

	@Override
	public long getItemId(int position) {
		return filteredResource.get(position).getId();
	}

	@NonNull
	@Override
	public Filter getFilter() {
		return new Filter() {
			@Override
			protected FilterResults performFiltering(CharSequence charSequence) {
				FilterResults results = new FilterResults();

				charSequence = charSequence.toString().toLowerCase();

				if (charSequence.length() == 0) {
					results.values = resource;
					results.count = resource.size();
				} else {
					List<T> filterResultsData = new ArrayList<>();

					for (T data : resource) {
						String title = data.getTitle().toLowerCase();

						if (title.contains(charSequence))
							filterResultsData.add(data);
					}

					results.values = filterResultsData;
					results.count = filterResultsData.size();
				}

				return results;
			}

			@Override
			protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
				filteredResource = (ArrayList<T>) filterResults.values;

				notifyDataSetChanged();
			}
		};
	}
}