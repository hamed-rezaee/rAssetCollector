package com.rezaee.rassetcollector.selectdialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filterable;
import android.widget.TextView;

import com.rezaee.rassetcollector.R;
import com.rezaee.rassetcollector.model.IFilterableMultiSelectClass;

import java.util.List;

public class CustomMultiSelectArrayAdapter<T extends IFilterableMultiSelectClass> extends com.rezaee.rassetcollector.selectdialog.CustomArrayAdapter<T> implements Filterable {
	public CustomMultiSelectArrayAdapter(Context context, List<T> objects) {
		super(context, R.layout.item_multi_view, objects);
	}

	@NonNull
	@Override
	public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
		final T data = getItem(position);

		LayoutInflater layoutInflater = LayoutInflater.from(getContext());
		View itemView = layoutInflater.inflate(R.layout.item_multi_view, parent, false);

		TextView txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
		txtTitle.setText(data.getTitle());

		CheckBox chkIsSelected = (CheckBox) itemView.findViewById(R.id.chkIsSelected);
		chkIsSelected.setChecked(data.isSelected());

		chkIsSelected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
				data.setSelected(b);
			}
		});

		return itemView;
	}
}