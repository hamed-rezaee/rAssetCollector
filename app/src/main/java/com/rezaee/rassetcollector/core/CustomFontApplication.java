package com.rezaee.rassetcollector.core;

import com.orm.SugarApp;
import com.rezaee.rassetcollector.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class CustomFontApplication extends SugarApp {
	@Override
	public void onCreate() {
		super.onCreate();

		CalligraphyConfig.initDefault(
				new CalligraphyConfig.Builder()
						.setDefaultFontPath(getResources().getString(R.string.FontPath))
						.setFontAttrId(R.attr.fontPath)
						.build()
		);
	}
}