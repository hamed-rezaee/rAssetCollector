package com.rezaee.rassetcollector.core;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.github.angads25.filepicker.controller.DialogSelectionListener;
import com.github.angads25.filepicker.model.DialogConfigs;
import com.github.angads25.filepicker.model.DialogProperties;
import com.github.angads25.filepicker.view.FilePickerDialog;
import com.rezaee.rassetcollector.Property.PropertyListFragment;
import com.rezaee.rassetcollector.R;
import com.rezaee.rassetcollector.location.LocationListFragment;
import com.rezaee.rassetcollector.person.PersonListFragment;
import com.rezaee.rassetcollector.propertyassign.PropertyAssignListFragment;
import com.rezaee.rassetcollector.propertystatus.PropertyStatusListFragment;
import com.rezaee.rassetcollector.propertytype.PropertyTypeListFragment;

import java.io.File;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
	private static final int REQUEST_CODE = 13640820;
	private static final int NOTIFICATION_ID = 13640821;

	private SharedPreferences sharedPreferences;

	private Toolbar tlbToolbarMain;
	private DrawerLayout drlMainDrawerLayout;
	private NavigationView nvuMainNavigationView;
	private CollapsingToolbarLayout ctlMainCollapsingToolbarLayout;

	private boolean showIconInNotification = false;
	private boolean doubleBackToExitPressedOnce = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
			.setDefaultFontPath(getResources().getString(R.string.FontPath))
			.setFontAttrId(R.attr.fontPath)
			.build()
		);

		sharedPreferences = getSharedPreferences(Constants.PREFERENCES, Context.MODE_PRIVATE);

		Typeface collapsingToolbarTypeface = TypefaceUtils.load(getBaseContext().getAssets(), getString(R.string.FontPath));

		tlbToolbarMain = (Toolbar) findViewById(R.id.tlbToolbarMain);
		drlMainDrawerLayout = (DrawerLayout) findViewById(R.id.drlMainDrawerLayout);
		nvuMainNavigationView = (NavigationView) findViewById(R.id.nvuMainNavigationView);
		ctlMainCollapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.ctlMainCollapsingToolbarLayout);

		setSupportActionBar(tlbToolbarMain);

		ctlMainCollapsingToolbarLayout.setCollapsedTitleTypeface(collapsingToolbarTypeface);
		ctlMainCollapsingToolbarLayout.setExpandedTitleTypeface(collapsingToolbarTypeface);

		ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drlMainDrawerLayout, tlbToolbarMain, R.string.Open, R.string.Close);
		drlMainDrawerLayout.addDrawerListener(actionBarDrawerToggle);
		actionBarDrawerToggle.syncState();

		nvuMainNavigationView.setNavigationItemSelectedListener(this);

		showIconInNotification = sharedPreferences.getBoolean(Constants.SHOW_ICON_IN_NOTIFICATION, false);

		if (showIconInNotification)
			showNotification();
		else
			dismissNotification();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_main, menu);

		MenuItem itmShowIconInNotification = menu.findItem(R.id.itmShowIconInNotification);
		itmShowIconInNotification.setChecked(showIconInNotification);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.itmShowIconInNotification:
				item.setChecked(!item.isChecked());

				SharedPreferences.Editor editor = sharedPreferences.edit();
				editor.putBoolean(Constants.SHOW_ICON_IN_NOTIFICATION, item.isChecked());
				editor.apply();

				if (item.isChecked())
					showNotification();
				else
					dismissNotification();

				return true;
			default:
				return false;
		}
	}

	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
	}

	@Override
	public void onBackPressed() {
		if (drlMainDrawerLayout.isDrawerOpen(GravityCompat.START))
			drlMainDrawerLayout.closeDrawer(GravityCompat.START);
		else if (isTaskRoot())
			exitApplication();
		else
			super.onBackPressed();
	}

	@Override
	public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
		Helpers.HideDrawerLayout(drlMainDrawerLayout);

		switch (menuItem.getItemId()) {
			case R.id.itmPropertyAssignList:
				showEntityList(new PropertyAssignListFragment());
				break;

			case R.id.itmPropertyList:
				showEntityList(new PropertyListFragment());
				break;

			case R.id.itmLocationList:
				showEntityList(new LocationListFragment());
				break;

			case R.id.itmPropertyTypeList:
				showEntityList(new PropertyTypeListFragment());
				break;

			case R.id.itmPropertyStatusList:
				showEntityList(new PropertyStatusListFragment());
				break;

			case R.id.itmPersonList:
				showEntityList(new PersonListFragment());
				break;

			case R.id.itmExportDatabase:
				exportDatabase();
				break;

			case R.id.itmImportDatabase:
				importDatabase();
				break;

			case R.id.itmAboutApplication:
				showAboutApplication();
				break;

			case R.id.itmExitApplication:
				finish();
				break;
		}

		return true;
	}

	private void showEntityList(Fragment fragment) {
		FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.frlMainContainer, fragment);
		fragmentTransaction.commit();
	}

	private void exportDatabase() {
		DialogProperties properties = new DialogProperties();

		properties.selection_mode = DialogConfigs.SINGLE_MODE;
		properties.selection_type = DialogConfigs.DIR_SELECT;
		properties.root = new File(DialogConfigs.DEFAULT_DIR);
		properties.error_dir = new File(DialogConfigs.DEFAULT_DIR);
		properties.offset = new File(DialogConfigs.DEFAULT_DIR);
		properties.extensions = new String[]{"db"};

		FilePickerDialog dialog = new FilePickerDialog(MainActivity.this, properties);

		dialog.setTitle(getResources().getString(R.string.ExportDatabase));
		dialog.setPositiveBtnName(getResources().getString(R.string.Ok));
		dialog.setNegativeBtnName(getResources().getString(R.string.Cancel));

		dialog.setDialogSelectionListener(new DialogSelectionListener() {
			@Override
			public void onSelectedFilePaths(String[] files) {
				Helpers.ExportDatabase(findViewById(R.id.clyMainCoordinatorLayout), files[0]);
			}
		});

		dialog.show();
	}

	private void importDatabase() {
		DialogProperties properties = new DialogProperties();

		properties.selection_mode = DialogConfigs.SINGLE_MODE;
		properties.selection_type = DialogConfigs.FILE_SELECT;
		properties.root = new File(DialogConfigs.DEFAULT_DIR);
		properties.error_dir = new File(DialogConfigs.DEFAULT_DIR);
		properties.offset = new File(DialogConfigs.DEFAULT_DIR);
		properties.extensions = new String[]{"db"};

		FilePickerDialog dialog = new FilePickerDialog(MainActivity.this, properties);

		dialog.setTitle(getResources().getString(R.string.ImportDatabase));
		dialog.setPositiveBtnName(getResources().getString(R.string.Ok));
		dialog.setNegativeBtnName(getResources().getString(R.string.Cancel));

		dialog.setDialogSelectionListener(new DialogSelectionListener() {
			@Override
			public void onSelectedFilePaths(String[] files) {
				Helpers.ImportDatabase(findViewById(R.id.clyMainCoordinatorLayout), files[0]);
			}
		});

		dialog.show();
	}

	private void showAboutApplication() {
		AboutDialogFragment aboutDialogFragment = new AboutDialogFragment();
		aboutDialogFragment.show(this.getSupportFragmentManager(), Constants.ABOUT_DIALOG_FRAGMENT);
	}

	private void exitApplication() {
		if (doubleBackToExitPressedOnce) {
			super.onBackPressed();
			return;
		}

		doubleBackToExitPressedOnce = true;

		Toast.makeText(this, R.string.PleaseClickAgainToExit, Toast.LENGTH_SHORT).show();

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				doubleBackToExitPressedOnce = false;
			}
		}, Constants.EXIT_CONFIRM_TIMEOUT);
	}

	public void setActionBarTitle(String title) {
		tlbToolbarMain.setTitle(title);
		ctlMainCollapsingToolbarLayout.setTitle(title);
	}

	private void showNotification() {
		NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

		Intent intent = new Intent(this, MainActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, REQUEST_CODE, intent, PendingIntent.FLAG_CANCEL_CURRENT);

		builder.setContentIntent(pendingIntent);
		builder.setContentTitle("\t" + getResources().getString(R.string.app_name));
		builder.setSubText("\t\t" + getResources().getString(R.string.ClickForOpenApp));
		builder.setSmallIcon(R.drawable.ic_notification);
		builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));
		builder.setAutoCancel(false);
		builder.setShowWhen(false);

		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		notificationManager.notify(NOTIFICATION_ID, builder.build());
	}

	private void dismissNotification() {
		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		notificationManager.cancel(NOTIFICATION_ID);
	}
}