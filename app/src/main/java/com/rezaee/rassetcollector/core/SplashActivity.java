package com.rezaee.rassetcollector.core;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.rezaee.rassetcollector.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SplashActivity extends AppCompatActivity {
	private TextView lblAppName;
	private TextView lblVersion;
	private TextView lblAboutDeveloper;

	private ImageView imgAppLogo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
				.setDefaultFontPath(getResources().getString(R.string.FontPath))
				.setFontAttrId(R.attr.fontPath)
				.build()
		);

		Animation myFadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_anim);

		lblAppName = (TextView) findViewById(R.id.lblAppName);
		lblVersion = (TextView) findViewById(R.id.lblVersion);
		lblAboutDeveloper = (TextView) findViewById(R.id.lblAboutDeveloper);

		imgAppLogo = (ImageView) findViewById(R.id.imgAppLogo);

		lblAppName.startAnimation(myFadeInAnimation);
		lblVersion.startAnimation(myFadeInAnimation);
		lblAboutDeveloper.startAnimation(myFadeInAnimation);

		imgAppLogo.startAnimation(myFadeInAnimation);

		myFadeInAnimation.setAnimationListener(new Animation.AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				if (getIntent().getBooleanExtra("startMainIntent", true)) {
					Intent intent = new Intent(SplashActivity.this, MainActivity.class);
					startActivity(intent);
				}

				finish();
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}
		});
	}

	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
	}
}