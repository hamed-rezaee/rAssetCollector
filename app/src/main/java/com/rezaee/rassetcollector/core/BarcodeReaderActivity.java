package com.rezaee.rassetcollector.core;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import com.google.zxing.Result;
import com.rezaee.rassetcollector.R;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class BarcodeReaderActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

	private ZXingScannerView scannerView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		scannerView = new ZXingScannerView(this);

		setContentView(scannerView);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !checkPermission()) {
			requestPermission();
		}
	}

	private boolean checkPermission() {
		return ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
	}

	private void requestPermission() {
		ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, Constants.REQUEST_CAMERA);
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			if (checkPermission()) {
				if (scannerView == null) {
					scannerView = new ZXingScannerView(this);
					setContentView(scannerView);
				}

				scannerView.setResultHandler(this);
				scannerView.startCamera();
			} else {
				requestPermission();
			}
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		scannerView.stopCamera();
	}

	@Override
	public void handleResult(final Result result) {
		final String scanResult = result.getText();

		Intent resultIntent = new Intent();
		resultIntent.putExtra(Constants.BARCODE_READER_RESULT, scanResult);

		setResult(Activity.RESULT_OK, resultIntent);

		finish();
	}
}