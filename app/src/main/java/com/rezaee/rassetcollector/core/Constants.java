package com.rezaee.rassetcollector.core;

public class Constants {
	public static final String EMPTY_STRING = "";
	public static final String DATE_FORMAT = "yyyy/MM/dd";

	public static final String PREFERENCES = "RAssetCollectorPreferences";

	public static final String SHOW_ICON_IN_NOTIFICATION = "ShowIconInNotification";

	public static final String ABOUT_DIALOG_FRAGMENT = "AboutDialogFragment";

	public static final String DEFINE_PERSON_FRAGMENT = "DefinePersonFragment";
	public static final String DEFINE_PROPERTY_FRAGMENT = "DefinePropertyFragment";
	public static final String DEFINE_PROPERTY_ASSIGN_FRAGMENT = "DefinePropertyAssignFragment";
	public static final String DEFINE_PROPERTY_TYPE_FRAGMENT = "DefinePropertyTypeFragment";
	public static final String DEFINE_PROPERTY_STATUS_FRAGMENT = "DefinePropertyStatusFragment";
	public static final String DEFINE_LOCATION_FRAGMENT = "DefineLocationFragment";

	public static final String SELECT_DIALOG_FRAGMENT = "SelectDialogFragment";

	public static final String DATE_PICKER_FRAGMENT = "DatePickerFragment";

	public static final String BARCODE_READER_RESULT = "DatePickerFragment";

	public static final int REQUEST_CAMERA = 4568;
	public static final int REQUEST_CODE = 4569;

	public static final int BARCODE_READER_RESULT_CODE = 1025;
	public static final int EXIT_CONFIRM_TIMEOUT = 2000;
	public static final int BUFFER_SIZE = 1024;

	public static final String PERSON_SEPARATOR = " | ";

	public enum StorageOperationMode {
		FromExternal,
		ToExternal
	}
}