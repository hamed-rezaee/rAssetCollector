package com.rezaee.rassetcollector.core;

import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.provider.ContactsContract;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.alirezaafkar.sundatepicker.DatePicker;
import com.alirezaafkar.sundatepicker.interfaces.DateSetListener;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.rezaee.rassetcollector.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;

import ir.anamsoftware.persiandateultimate.ManamPDUltimate;
import uk.co.chrisjenx.calligraphy.CalligraphyTypefaceSpan;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

public class Helpers {
	public static String FormatDate(Date date) {
		return new SimpleDateFormat(Constants.DATE_FORMAT).format(date);
	}

	public static Date GetDate(String date) {
		Date result = null;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);

		try {
			result = simpleDateFormat.parse(date);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public static Calendar GetCalendar(String date) {
		return GetCalendar(GetDate(date));
	}

	public static Calendar GetCalendar(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		return calendar;
	}

	public static void ExportDatabase(View view, String path) {
		try {
			copyDatabase(view, path, Constants.StorageOperationMode.ToExternal);
		} catch (IOException ignored) {
		}
	}

	public static void ImportDatabase(View view, String path) {
		try {
			copyDatabase(view, path, Constants.StorageOperationMode.FromExternal);
		} catch (IOException ignored) {
		}
	}

	private static void copyDatabase(View view, String path, Constants.StorageOperationMode storageOperationMode) throws IOException {
		final String packageName = view.getContext().getResources().getString(R.string.PackageName);
		final String databaseName = view.getContext().getResources().getString(R.string.DatabaseName);
		final String databaseFilePath = String.format("data/data/%s/databases/%s", packageName, databaseName);

		String targetFileName = Constants.EMPTY_STRING, sourceFileName = Constants.EMPTY_STRING;

		if (storageOperationMode == Constants.StorageOperationMode.ToExternal) {
			sourceFileName = databaseFilePath;
			targetFileName = String.format("%s/%s", path, "DatabaseBackup_" + UUID.randomUUID() + ".db");
		} else if (storageOperationMode == Constants.StorageOperationMode.FromExternal) {
			sourceFileName = path;
			targetFileName = databaseFilePath;
		}

		try {
			int dataLength;
			byte[] dataBuffer = new byte[Constants.BUFFER_SIZE];

			OutputStream outputStream = new FileOutputStream(targetFileName);
			FileInputStream inputStream = new FileInputStream(new File(sourceFileName));

			while ((dataLength = inputStream.read(dataBuffer)) > 0)
				outputStream.write(dataBuffer, 0, dataLength);

			outputStream.flush();
			outputStream.close();
			inputStream.close();

			final Snackbar snackbar = Snackbar.make(view, R.string.OperationSuccessfullyCompleted, Snackbar.LENGTH_SHORT);

			snackbar.setAction(R.string.Ok, new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					snackbar.dismiss();
				}
			}).show();
		} catch (IOException e) {
			final Snackbar snackbar = Snackbar.make(view, R.string.OperationFailed, Snackbar.LENGTH_SHORT);

			snackbar.setAction(R.string.Ok, new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					snackbar.dismiss();
				}
			}).show();
		}
	}

	public static void ExpandAppBar(Activity activity, boolean expand) {
		AppBarLayout ablMainAppBarLayout = (AppBarLayout) activity.findViewById(R.id.ablMainAppBarLayout);
		ablMainAppBarLayout.setExpanded(expand);
	}

	public static void SetFABVisibility(Activity activity, int viewState) {
		FloatingActionButton fabAddItem = (FloatingActionButton) activity.findViewById(R.id.fabAddItem);

		fabAddItem.setVisibility(viewState);
	}

	public static Spannable WrapInCustomFont(Context context, android.support.v4.app.Fragment fragment, String text) {
		Typeface typeface = TypefaceUtils.load(context.getAssets(), fragment.getString(R.string.FontPath));

		CalligraphyTypefaceSpan typefaceSpan = new CalligraphyTypefaceSpan(typeface);
		SpannableString spannable = new SpannableString(text);
		spannable.setSpan(typefaceSpan, 0, text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

		return spannable;
	}

	public static void HideKeypad(Activity activity, View view) {
		try {
			final InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(android.content.Context.INPUT_METHOD_SERVICE);

			inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
		} catch (Exception ignored) {
		}
	}

	public static DatePicker ShowPersianDatePicker(final TextView textView) {
		return new DatePicker.Builder()
			.date(Helpers.GetCalendar(new ManamPDUltimate().PersianToGregorian(textView.getText().toString())))
			.build(new DateSetListener() {
				@Override
				public void onDateSet(int id, Calendar calendar, int day, int month, int year) {
					textView.setText(year + "/" + month + "/" + day);
				}
			});
	}

	public static String FormatLongValue(long number) {
		return FormatLongValue(String.valueOf(number));
	}

	public static String FormatLongValue(String number) {
		try {
			return String.format("%,d", Long.valueOf(number));
		} catch (Exception e) {
			return "0";
		}
	}

	public static String CleanCommaSeparatedValue(String commaSeparatedValue) {
		try {
			return commaSeparatedValue.replace(",", Constants.EMPTY_STRING);
		} catch (Exception e) {
			return Constants.EMPTY_STRING;
		}
	}

	public static void ShowDrawerLayout(DrawerLayout drawerLayout) {
		drawerLayout.openDrawer(GravityCompat.START);
	}

	public static void HideDrawerLayout(DrawerLayout drawerLayout) {
		drawerLayout.closeDrawer(GravityCompat.START);
	}

	public static void InitializePieChart(PieChart pieChart, Typeface typeface, String label, List<Long> inputValues, List<String> inputLabels) {
		pieChart.getDescription().setEnabled(false);
		pieChart.setRotationEnabled(true);
		pieChart.setCenterText(label);
		pieChart.setCenterTextSize(10);
		pieChart.setCenterTextTypeface(typeface);
		pieChart.setHoleRadius(55);
		pieChart.setTransparentCircleAlpha(100);
		pieChart.setTransparentCircleRadius(60);

		addDataSetToChart(pieChart, label, inputValues, inputLabels, typeface);
	}

	private static void addDataSetToChart(PieChart pieChart, String label, List<Long> inputData, List<String> inputLabels, Typeface typeface) {
		ArrayList<PieEntry> data = new ArrayList<>();
		ArrayList<Integer> colors = new ArrayList<>();
		ArrayList<LegendEntry> legendEntry = new ArrayList<>();

		for (int i = 0; i < inputData.size(); i++) {
			int color = generateRandomColor();

			data.add(new PieEntry(inputData.get(i), i));
			legendEntry.add(new LegendEntry(inputLabels.get(i), Legend.LegendForm.SQUARE, 7, 7, null, color));
			colors.add(color);
		}

		PieDataSet pieDataSet = new PieDataSet(data, label);
		pieDataSet.setColors(colors);
		pieDataSet.setSliceSpace(1);
		pieDataSet.setValueTextSize(8);
		pieDataSet.setValueTextColor(Color.WHITE);
		pieDataSet.setValueTypeface(typeface);

		Legend legend = pieChart.getLegend();
		legend.setTypeface(typeface);
		legend.setCustom(legendEntry);
		legend.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
		legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
		legend.setOrientation(Legend.LegendOrientation.VERTICAL);

		pieChart.setData(new PieData(pieDataSet));
		pieChart.invalidate();
	}

	private static int generateRandomColor() {
		Random randomGenerator = new Random();

		int red = randomGenerator.nextInt(128) + 128;
		int green = randomGenerator.nextInt(128) + 128;
		int blue = randomGenerator.nextInt(128) + 128;

		return Color.rgb(red, green, blue);
	}

	public static byte[] BitmapToByteArray(Bitmap bitmap) {
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 0, byteArrayOutputStream);

		return byteArrayOutputStream.toByteArray();
	}

	public static Bitmap ByteArrayToBitmap(byte[] data) {
		return BitmapFactory.decodeByteArray(data, 0, data.length, new BitmapFactory.Options());
	}

	public static Bitmap ImageViewToBitmap(ImageView imageView) {
		return ((BitmapDrawable) imageView.getDrawable()).getBitmap();
	}

	private static Bitmap getContactPhoto(Context context, String contactID) {
		try {
			InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(context.getContentResolver(), ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.valueOf(contactID)));

			if (inputStream != null)
				return BitmapFactory.decodeStream(inputStream);
		} catch (Exception ignored) {
		}

		return BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_contact_default);
	}

	public static void SetLanguage(Context context, String language) {
		Locale locale = new Locale(language);

		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.setLocale(locale);

		context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
	}
}