package com.rezaee.rassetcollector.Property;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;

import com.rezaee.rassetcollector.R;
import com.rezaee.rassetcollector.core.BarcodeReaderActivity;
import com.rezaee.rassetcollector.core.Constants;
import com.rezaee.rassetcollector.core.Helpers;
import com.rezaee.rassetcollector.core.MainActivity;
import com.rezaee.rassetcollector.model.Property;
import com.rezaee.rassetcollector.model.PropertyStatus;
import com.rezaee.rassetcollector.model.PropertyType;
import com.rezaee.rassetcollector.selectdialog.PropertyStatusSelectDialogFragment;
import com.rezaee.rassetcollector.selectdialog.PropertyTypeSelectDialogFragment;

import java.util.Date;

import ir.anamsoftware.persiandateultimate.ManamPDUltimate;

public class PropertyDefineFragment extends Fragment {
	EditText txtTitle;
	EditText txtPropertyCode;
	EditText txtPropertyType;
	EditText txtPropertyStatus;
	EditText txtRegisterDate;
	EditText txtComment;

	ImageView imgPropertyCode;
	ImageView imgPropertyType;
	ImageView imgPropertyStatus;
	ImageView imgRegisterDate;

	TextInputLayout tilTitle;
	TextInputLayout tilPropertyCode;
	TextInputLayout tilPropertyType;
	TextInputLayout tilPropertyStatus;
	TextInputLayout tilRegisterDate;

	CheckBox chkActive;

	private static Property property;

	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		((MainActivity) getActivity()).setActionBarTitle(getContext().getResources().getString(R.string.Property));

		View view = inflater.inflate(R.layout.fragment_define_property, container, false);

		initialForm(view);

		Button cmdSave = (Button) view.findViewById(R.id.cmdSave);
		Button cmdCancel = (Button) view.findViewById(R.id.cmdCancel);

		cmdSave.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (validateSave()) {
					saveForm();

					getActivity().getSupportFragmentManager()
						.beginTransaction()
						.replace(R.id.frlMainContainer, new PropertyListFragment())
						.commit();

					Helpers.HideKeypad(getActivity(), view);

					((MainActivity) getActivity()).setActionBarTitle(getContext().getResources().getString(R.string.app_name));

					final Snackbar snackbar = Snackbar.make(view, R.string.OperationSuccessfullyCompleted, Snackbar.LENGTH_SHORT);

					snackbar.setAction(R.string.Ok, new View.OnClickListener() {
						@Override
						public void onClick(View view) {
							snackbar.dismiss();
						}
					}).show();
				}
			}
		});

		cmdCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(final View view) {
				getActivity().getSupportFragmentManager()
					.beginTransaction()
					.replace(R.id.frlMainContainer, new PropertyListFragment())
					.commit();

				Helpers.HideKeypad(getActivity(), view);
			}
		});

		imgPropertyCode.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				showBarcodeReader();
			}
		});

		txtPropertyType.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View view, boolean hasFocus) {
				if (hasFocus)
					showSelectDialog(new PropertyTypeSelectDialogFragment(), false);
			}
		});

		txtPropertyType.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				showSelectDialog(new PropertyTypeSelectDialogFragment(), false);
			}
		});

		imgPropertyType.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				showSelectDialog(new PropertyTypeSelectDialogFragment(), false);
			}
		});

		txtPropertyStatus.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View view, boolean hasFocus) {
				if (hasFocus)
					showSelectDialog(new PropertyStatusSelectDialogFragment(), false);
			}
		});

		txtPropertyStatus.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				showSelectDialog(new PropertyStatusSelectDialogFragment(), false);
			}
		});

		imgPropertyStatus.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				showSelectDialog(new PropertyStatusSelectDialogFragment(), false);
			}
		});


		txtRegisterDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View view, boolean hasFocus) {
				if (hasFocus)
					Helpers.ShowPersianDatePicker(txtRegisterDate).show(getFragmentManager(), Constants.DATE_PICKER_FRAGMENT);
			}
		});

		txtRegisterDate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Helpers.ShowPersianDatePicker(txtRegisterDate).show(getFragmentManager(), Constants.DATE_PICKER_FRAGMENT);
			}
		});

		imgRegisterDate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Helpers.ShowPersianDatePicker(txtRegisterDate).show(getFragmentManager(), Constants.DATE_PICKER_FRAGMENT);
			}
		});

		Helpers.ExpandAppBar(getActivity(), false);
		Helpers.SetFABVisibility(getActivity(), View.GONE);

		return view;
	}

	private void showBarcodeReader() {
		Intent intent = new Intent(getActivity(), BarcodeReaderActivity.class);
		startActivityForResult(intent, Constants.BARCODE_READER_RESULT_CODE);
	}

	private void showSelectDialog(DialogFragment dialogFragment, boolean cancelable) {
		dialogFragment.setCancelable(cancelable);
		dialogFragment.setTargetFragment(this, Constants.REQUEST_CODE);
		dialogFragment.show(getActivity().getSupportFragmentManager(), Constants.SELECT_DIALOG_FRAGMENT);
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
			case Constants.BARCODE_READER_RESULT_CODE:
				if (resultCode == android.app.Activity.RESULT_OK) {
					txtPropertyCode.setText(data.getExtras().getString(Constants.BARCODE_READER_RESULT, Constants.EMPTY_STRING));
				}
				break;
		}
	}

	private void saveForm() {
		Property tempProperty;
		ManamPDUltimate manamPDUltimate = new ManamPDUltimate();

		Date registerDate = Helpers.GetDate(manamPDUltimate.PersianToGregorian(txtRegisterDate.getText().toString()));

		if (property.getId() != null && property.getId() > 0) {
			tempProperty = Property.findById(Property.class, property.getId());

			tempProperty.SetData(tempProperty, txtTitle.getText().toString(), txtPropertyCode.getText().toString(), property.getPropertyTypeID(), property.getPropertyStatusID(), registerDate, txtComment.getText().toString(), chkActive.isChecked());
		} else {
			tempProperty = new Property(txtTitle.getText().toString(), txtPropertyCode.getText().toString(), property.getPropertyTypeID(), property.getPropertyStatusID(), registerDate, txtComment.getText().toString(), chkActive.isChecked());
		}

		tempProperty.save();
	}

	private void initialForm(View view) {
		ManamPDUltimate manamPDUltimate = new ManamPDUltimate();

		txtTitle = (EditText) view.findViewById(R.id.txtTitle);
		txtPropertyCode = (EditText) view.findViewById(R.id.txtPropertyCode);
		txtPropertyType = (EditText) view.findViewById(R.id.txtPropertyType);
		txtPropertyStatus = (EditText) view.findViewById(R.id.txtPropertyStatus);
		txtRegisterDate = (EditText) view.findViewById(R.id.txtRegisterDate);
		txtComment = (EditText) view.findViewById(R.id.txtComment);


		imgPropertyCode = (ImageView) view.findViewById(R.id.imgPropertyCode);
		imgPropertyType = (ImageView) view.findViewById(R.id.imgPropertyType);
		imgPropertyStatus = (ImageView) view.findViewById(R.id.imgPropertyStatus);
		imgRegisterDate = (ImageView) view.findViewById(R.id.imgRegisterDate);

		tilTitle = (TextInputLayout) view.findViewById(R.id.tilTitle);
		tilPropertyCode = (TextInputLayout) view.findViewById(R.id.tilPropertyCode);
		tilPropertyType = (TextInputLayout) view.findViewById(R.id.tilPropertyType);
		tilPropertyStatus = (TextInputLayout) view.findViewById(R.id.tilPropertyStatus);
		tilRegisterDate = (TextInputLayout) view.findViewById(R.id.tilRegisterDate);

		chkActive = (CheckBox) view.findViewById(R.id.chkActive);

		if (property == null)
			property = new Property();

		txtTitle.setText(property.getTitle());
		txtPropertyCode.setText(property.getCode());
		try {
			txtPropertyType.setText(PropertyType.findById(PropertyType.class, property.getPropertyTypeID()).getTitle());
		} catch (Exception ignored) {
		}
		try {
			txtPropertyStatus.setText(PropertyStatus.findById(PropertyStatus.class, property.getPropertyStatusID()).getTitle());
		} catch (Exception ignored) {
		}
		try {
			txtRegisterDate.setText(manamPDUltimate.GregorianToPersian(Helpers.FormatDate(property.getRegisterDate())));
		} catch (Exception ignored) {
		}
		txtComment.setText(property.getComment());

		chkActive.setChecked(property.isActive());

		txtTitle.requestFocus();
	}

	private boolean validateSave() {
		if (txtTitle.getText().toString().isEmpty()) {
			tilTitle.setError(Helpers.WrapInCustomFont(getContext(), this, getString(R.string.RequiredField)));
			txtTitle.requestFocus();

			return false;
		} else {
			tilTitle.setErrorEnabled(false);
		}

		if (txtPropertyCode.getText().toString().isEmpty()) {
			tilPropertyCode.setError(Helpers.WrapInCustomFont(getContext(), this, getString(R.string.RequiredField)));
			txtPropertyCode.requestFocus();

			return false;
		} else {
			tilPropertyCode.setErrorEnabled(false);
		}

		if (txtPropertyType.getText().toString().isEmpty()) {
			tilPropertyType.setError(Helpers.WrapInCustomFont(getContext(), this, getString(R.string.RequiredField)));
			txtPropertyType.requestFocus();

			return false;
		} else {
			tilPropertyType.setErrorEnabled(false);
		}

		if (txtPropertyStatus.getText().toString().isEmpty()) {
			tilPropertyStatus.setError(Helpers.WrapInCustomFont(getContext(), this, getString(R.string.RequiredField)));
			txtPropertyStatus.requestFocus();

			return false;
		} else {
			tilPropertyStatus.setErrorEnabled(false);
		}

		if (txtRegisterDate.getText().toString().isEmpty()) {
			tilRegisterDate.setError(Helpers.WrapInCustomFont(getContext(), this, getString(R.string.RequiredField)));
			txtRegisterDate.requestFocus();

			return false;
		} else {
			tilRegisterDate.setErrorEnabled(false);
		}

		return true;
	}

	public void SetData(Property property) {
		PropertyDefineFragment.property = property;
	}

	public void SetPropertyType(PropertyType propertyType) {
		property.setPropertyTypeID(propertyType.getId());
		txtPropertyType.setText(propertyType.getTitle());
	}

	public void SetPropertyStatus(PropertyStatus propertyStatus) {
		property.setPropertyStatusID(propertyStatus.getId());
		txtPropertyStatus.setText(propertyStatus.getTitle());
	}
}