package com.rezaee.rassetcollector.Property;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.orm.SugarRecord;
import com.rezaee.rassetcollector.R;
import com.rezaee.rassetcollector.core.Helpers;
import com.rezaee.rassetcollector.core.MainActivity;
import com.rezaee.rassetcollector.model.Property;
import com.rezaee.rassetcollector.model.PropertyAssign;
import com.rezaee.rassetcollector.model.PropertyStatus;
import com.rezaee.rassetcollector.model.PropertyType;

import java.util.ArrayList;
import java.util.List;

import ir.anamsoftware.persiandateultimate.ManamPDUltimate;

public class PropertyRecyclerAdapter extends RecyclerView.Adapter<PropertyRecyclerAdapter.PropertyViewHolder> implements Filterable {
	private Context context;
	private List<Property> resource;
	private List<Property> filteredResource;
	private LayoutInflater layoutInflater;

	public PropertyRecyclerAdapter(Context context, List<Property> properties) {
		this.context = context;
		this.resource = properties;
		this.filteredResource = properties;
		this.layoutInflater = LayoutInflater.from(context);
	}

	@Override
	public PropertyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = layoutInflater.inflate(R.layout.item_property, parent, false);

		return new PropertyViewHolder(view);
	}

	@Override
	public void onBindViewHolder(PropertyViewHolder holder, int position) {
		holder.SetData(filteredResource.get(position));
	}

	@Override
	public int getItemCount() {
		return filteredResource.size();
	}

	public void deleteItem(final Property item, final int currentPosition) {
		new AlertDialog.Builder(context)
			.setTitle(R.string.Caution)
			.setMessage(R.string.AreYouSure)
			.setIcon(R.drawable.ic_caution)
			.setPositiveButton(R.string.Ok, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					if (validateDelete(item)) {
						Property.delete(item);

						filteredResource = SugarRecord.listAll(Property.class);

						notifyItemRemoved(currentPosition);
					}

					notifyItemRangeChanged(currentPosition, filteredResource.size());
				}
			})
			.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					notifyItemRangeChanged(currentPosition, filteredResource.size());
				}
			})
			.show();
	}

	private boolean validateDelete(Property property) {
		long resultCount = PropertyAssign.count(PropertyAssign.class, "property_id = ?", new String[]{String.valueOf(property.getId())});

		if (resultCount > 0) {
			Toast.makeText(context, R.string.ItemIsInUse, Toast.LENGTH_LONG).show();

			return false;
		}

		return true;
	}

	public void editItem(Property property) {
		PropertyDefineFragment propertyDefineFragment = new PropertyDefineFragment();

		propertyDefineFragment.SetData(property);

		FragmentTransaction fragmentTransaction = ((MainActivity) context).getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.frlMainContainer, propertyDefineFragment);
		fragmentTransaction.commit();
	}

	@Override
	public Filter getFilter() {
		return new Filter() {
			@Override
			protected FilterResults performFiltering(CharSequence charSequence) {
				FilterResults results = new FilterResults();

				charSequence = charSequence.toString().toLowerCase();

				if (charSequence.length() == 0) {
					results.values = resource;
					results.count = resource.size();
				} else {
					List<Property> filterResultsData = new ArrayList<>();

					for (Property data : resource) {
						String title = data.getTitle().toLowerCase();

						if (title.contains(charSequence))
							filterResultsData.add(data);
					}

					results.values = filterResultsData;
					results.count = filterResultsData.size();
				}

				return results;
			}

			@Override
			protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
				filteredResource = (ArrayList<Property>) filterResults.values;

				notifyDataSetChanged();
			}
		};
	}

	class PropertyViewHolder extends RecyclerView.ViewHolder {
		public Property currentObject;

		private TextView lblPropertyTitle;
		private TextView lblPropertyCode;
		private TextView lblPropertyTypeTitle;
		private TextView lblPropertyStatusTitle;
		private TextView lblRegisterDate;
		private TextView lblComment;

		public PropertyViewHolder(View itemView) {
			super(itemView);

			lblPropertyTitle = (TextView) itemView.findViewById(R.id.lblPropertyTitle);
			lblPropertyCode = (TextView) itemView.findViewById(R.id.lblPropertyCode);
			lblPropertyTypeTitle = (TextView) itemView.findViewById(R.id.lblPropertyTypeTitle);
			lblPropertyStatusTitle = (TextView) itemView.findViewById(R.id.lblPropertyStatusTitle);
			lblRegisterDate = (TextView) itemView.findViewById(R.id.lblRegisterDate);
			lblComment = (TextView) itemView.findViewById(R.id.lblComment);

			LinearLayout cmdEdit = (LinearLayout) itemView.findViewById(R.id.cmdEdit);
			LinearLayout cmdDelete = (LinearLayout) itemView.findViewById(R.id.cmdDelete);

			cmdEdit.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					editItem(currentObject);
				}
			});

			cmdDelete.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					deleteItem(currentObject, getAdapterPosition());
				}
			});
		}

		public void SetData(Property currentObject) {
			this.currentObject = currentObject;

			String date = new ManamPDUltimate().GregorianToPersian(Helpers.FormatDate(currentObject.getRegisterDate()));

			lblPropertyTitle.setText(currentObject.getTitle());
			lblPropertyCode.setText(currentObject.getCode());
			lblPropertyTypeTitle.setText(PropertyType.findById(PropertyType.class, currentObject.getPropertyTypeID()).getTitle());
			lblPropertyStatusTitle.setText(PropertyStatus.findById(PropertyStatus.class, currentObject.getPropertyStatusID()).getTitle());
			lblRegisterDate.setText(date);
			lblComment.setText(currentObject.getComment().isEmpty() ? context.getResources().getString(R.string.NoComment) : currentObject.getComment());
		}
	}
}