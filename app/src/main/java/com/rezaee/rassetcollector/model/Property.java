package com.rezaee.rassetcollector.model;

import com.orm.SugarRecord;
import com.rezaee.rassetcollector.core.Constants;

import java.util.Date;

public class Property extends SugarRecord {
	private String title;
	private String code;
	private long propertyTypeID;
	private long propertyStatusID;
	private Date registerDate;
	private String comment;
	private boolean active;

	public Property() {
		this.title = Constants.EMPTY_STRING;
		this.code = Constants.EMPTY_STRING;
		this.propertyTypeID = 0;
		this.propertyStatusID = 0;
		this.registerDate = new Date();
		this.comment = Constants.EMPTY_STRING;
		this.active = true;
	}

	public Property(String title, String code, long propertyTypeID, long propertyStatusID, Date registerDate, String comment, boolean active) {
		this.title = title;
		this.code = code;
		this.propertyTypeID = propertyTypeID;
		this.propertyStatusID = propertyStatusID;
		this.registerDate = registerDate;
		this.comment = comment;
		this.active = active;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public long getPropertyTypeID() {
		return propertyTypeID;
	}

	public void setPropertyTypeID(long propertyTypeID) {
		this.propertyTypeID = propertyTypeID;
	}

	public long getPropertyStatusID() {
		return propertyStatusID;
	}

	public void setPropertyStatusID(long propertyStatusID) {
		this.propertyStatusID = propertyStatusID;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Property SetData(Property property, String title, String code, long propertyTypeID, long propertyStatusID, Date registerDate, String comment, boolean active) {
		property.title = title;
		property.code = code;
		property.propertyTypeID = propertyTypeID;
		property.propertyStatusID = propertyStatusID;
		property.registerDate = registerDate;
		property.comment = comment;
		property.active = active;

		return property;
	}
}