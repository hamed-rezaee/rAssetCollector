package com.rezaee.rassetcollector.model;

import com.orm.SugarRecord;
import com.rezaee.rassetcollector.core.Constants;

public class PropertyAssign extends SugarRecord {
	private long personID;
	private long propertyID;
	private long locationID;
	private String comment;

	public PropertyAssign() {
		this.personID = 0;
		this.propertyID = 0;
		this.locationID = 0;
		this.comment = Constants.EMPTY_STRING;
	}

	public PropertyAssign(long personID, long propertyID, long locationID, String comment) {
		this.personID = personID;
		this.propertyID = propertyID;
		this.locationID = locationID;
		this.comment = comment;
	}

	public long getPersonID() {
		return personID;
	}

	public void setPersonID(long personID) {
		this.personID = personID;
	}

	public long getPropertyID() {
		return propertyID;
	}

	public void setPropertyID(long propertyID) {
		this.propertyID = propertyID;
	}

	public long getLocationID() {
		return locationID;
	}

	public void setLocationID(long locationID) {
		this.locationID = locationID;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public PropertyAssign SetData(PropertyAssign propertyAssign, long personID, long propertyID, long locationID, String comment) {
		propertyAssign.personID = personID;
		propertyAssign.propertyID = propertyID;
		propertyAssign.locationID = locationID;
		propertyAssign.comment = comment;

		return propertyAssign;
	}
}