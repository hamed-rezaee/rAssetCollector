package com.rezaee.rassetcollector.model;

public class CustomPerson implements IFilterableMultiSelectClass {
	private Long id;
	private String title;
	private boolean isSelected;

	public CustomPerson(long id, String title, boolean isSelected) {
		this.id = id;
		this.title = title;
		this.isSelected = isSelected;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean selected) {
		isSelected = selected;
	}
}