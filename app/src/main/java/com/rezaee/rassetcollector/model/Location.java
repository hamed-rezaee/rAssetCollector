package com.rezaee.rassetcollector.model;

import com.orm.SugarRecord;
import com.rezaee.rassetcollector.core.Constants;

public class Location extends SugarRecord implements IFilterableClass {
	private String title;
	private boolean active;

	public Location() {
		this.title = Constants.EMPTY_STRING;
		this.active = true;
	}

	public Location(String title, boolean active) {
		this.title = title;
		this.active = active;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Location SetData(Location location, String title, boolean active) {
		location.title = title;
		location.active = active;

		return location;
	}
}