package com.rezaee.rassetcollector.model;

public interface IFilterableMultiSelectClass extends IFilterableClass {
	boolean isSelected();

	void setSelected(boolean selected);
}