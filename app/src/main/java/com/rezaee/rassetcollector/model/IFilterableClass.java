package com.rezaee.rassetcollector.model;

public interface IFilterableClass {
	Long getId();

	String getTitle();
}