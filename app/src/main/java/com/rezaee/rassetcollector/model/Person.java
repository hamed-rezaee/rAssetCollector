package com.rezaee.rassetcollector.model;

import com.orm.SugarRecord;
import com.rezaee.rassetcollector.core.Constants;

import java.util.ArrayList;
import java.util.List;

public class Person extends SugarRecord implements IFilterableClass {
	private String firstName;
	private String lastName;
	private String accountNo;
	private String cardNo;

	public Person() {
		this.firstName = Constants.EMPTY_STRING;
		this.lastName = Constants.EMPTY_STRING;
		this.accountNo = Constants.EMPTY_STRING;
		this.cardNo = Constants.EMPTY_STRING;
	}

	public Person(String firstName, String lastName, String accountNo, String cardNo) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.accountNo = accountNo;
		this.cardNo = cardNo;
	}

	public static List<CustomPerson> GetCustomPersonsByPerson(List<Person> persons, boolean isSelected) {
		List<CustomPerson> customPersons = new ArrayList<>();

		for (Person person : persons)
			customPersons.add(GetCustomPerson(person.getId(), isSelected));

		return customPersons;
	}

	public static List<CustomPerson> GetCustomPersonsByPersonID(List<Integer> personIDs, boolean isSelected) {
		List<CustomPerson> customPersons = new ArrayList<>();

		for (long personID : personIDs)
			customPersons.add(GetCustomPerson(personID, isSelected));

		return customPersons;
	}

	private static CustomPerson GetCustomPerson(long personID, boolean isSelected) {
		CustomPerson customPerson;

		Person person = Person.findById(Person.class, personID);
		customPerson = new CustomPerson(person.getId(), person.getFullName(), isSelected);

		return customPerson;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getFullName() {
		return getFirstName() + " " + getLastName();
	}

	public Person SetData(Person person, String firstName, String lastName, String accountNo, String cardNo) {
		person.firstName = firstName;
		person.lastName = lastName;
		person.accountNo = accountNo;
		person.cardNo = cardNo;

		return person;
	}

	@Override
	public String getTitle() {
		return getFullName();
	}
}