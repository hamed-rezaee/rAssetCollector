package com.rezaee.rassetcollector.model;

import com.orm.SugarRecord;
import com.rezaee.rassetcollector.core.Constants;

public class PropertyStatus extends SugarRecord implements IFilterableClass {
	private String title;
	private boolean active;

	public PropertyStatus() {
		this.title = Constants.EMPTY_STRING;
		this.active = true;
	}

	public PropertyStatus(String title, boolean active) {
		this.title = title;
		this.active = active;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public PropertyStatus SetData(PropertyStatus propertyStatus, String title, boolean active) {
		propertyStatus.title = title;
		propertyStatus.active = active;

		return propertyStatus;
	}
}