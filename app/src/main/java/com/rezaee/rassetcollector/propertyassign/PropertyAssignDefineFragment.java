package com.rezaee.rassetcollector.propertyassign;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.rezaee.rassetcollector.R;
import com.rezaee.rassetcollector.core.BarcodeReaderActivity;
import com.rezaee.rassetcollector.core.Constants;
import com.rezaee.rassetcollector.core.Helpers;
import com.rezaee.rassetcollector.core.MainActivity;
import com.rezaee.rassetcollector.model.Location;
import com.rezaee.rassetcollector.model.Person;
import com.rezaee.rassetcollector.model.Property;
import com.rezaee.rassetcollector.model.PropertyAssign;
import com.rezaee.rassetcollector.selectdialog.LocationSelectDialogFragment;
import com.rezaee.rassetcollector.selectdialog.PersonSelectDialogFragment;

import java.util.List;

public class PropertyAssignDefineFragment extends Fragment {
	EditText txtPerson;
	EditText txtProperty;
	EditText txtLocation;
	EditText txtComment;

	ImageView imgPerson;
	ImageView imgProperty;
	ImageView imgLocation;

	TextInputLayout tilPerson;
	TextInputLayout tilProperty;
	TextInputLayout tilLocation;

	private static PropertyAssign propertyAssign;

	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		((MainActivity) getActivity()).setActionBarTitle(getContext().getResources().getString(R.string.PropertyAssign));

		View view = inflater.inflate(R.layout.fragment_define_property_assign, container, false);

		initialForm(view);

		Button cmdSave = (Button) view.findViewById(R.id.cmdSave);
		Button cmdCancel = (Button) view.findViewById(R.id.cmdCancel);

		cmdSave.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (validateSave()) {
					saveForm();

					getActivity().getSupportFragmentManager()
						.beginTransaction()
						.replace(R.id.frlMainContainer, new PropertyAssignListFragment())
						.commit();

					Helpers.HideKeypad(getActivity(), view);

					((MainActivity) getActivity()).setActionBarTitle(getContext().getResources().getString(R.string.app_name));

					final Snackbar snackbar = Snackbar.make(view, R.string.OperationSuccessfullyCompleted, Snackbar.LENGTH_SHORT);

					snackbar.setAction(R.string.Ok, new View.OnClickListener() {
						@Override
						public void onClick(View view) {
							snackbar.dismiss();
						}
					}).show();
				}
			}
		});

		cmdCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(final View view) {
				getActivity().getSupportFragmentManager()
					.beginTransaction()
					.replace(R.id.frlMainContainer, new PropertyAssignListFragment())
					.commit();

				Helpers.HideKeypad(getActivity(), view);
			}
		});

		txtPerson.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View view, boolean hasFocus) {
				if (hasFocus)
					showSelectDialog(new PersonSelectDialogFragment(), false);
			}
		});

		txtPerson.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				showSelectDialog(new PersonSelectDialogFragment(), false);
			}
		});

		imgPerson.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				showSelectDialog(new PersonSelectDialogFragment(), false);
			}
		});

		imgProperty.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				showBarcodeReader();
			}
		});

		txtLocation.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View view, boolean hasFocus) {
				if (hasFocus)
					showSelectDialog(new LocationSelectDialogFragment(), false);
			}
		});

		txtLocation.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				showSelectDialog(new LocationSelectDialogFragment(), false);
			}
		});

		imgLocation.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				showSelectDialog(new LocationSelectDialogFragment(), false);
			}
		});

		Helpers.ExpandAppBar(getActivity(), false);
		Helpers.SetFABVisibility(getActivity(), View.GONE);

		return view;
	}

	private void showBarcodeReader() {
		Intent intent = new Intent(getActivity(), BarcodeReaderActivity.class);
		startActivityForResult(intent, Constants.BARCODE_READER_RESULT_CODE);
	}

	private void showSelectDialog(DialogFragment dialogFragment, boolean cancelable) {
		dialogFragment.setCancelable(cancelable);
		dialogFragment.setTargetFragment(this, Constants.REQUEST_CODE);
		dialogFragment.show(getActivity().getSupportFragmentManager(), Constants.SELECT_DIALOG_FRAGMENT);
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
			case Constants.BARCODE_READER_RESULT_CODE:
				if (resultCode == android.app.Activity.RESULT_OK) {
					String propertyCode = data.getExtras().getString(Constants.BARCODE_READER_RESULT, Constants.EMPTY_STRING);

					List<Property> property = Property.find(Property.class, "code = ?", propertyCode);

					if (property.size() > 0) {
						propertyAssign.setPropertyID(property.get(0).getId());
						txtProperty.setText(property.get(0).getTitle() + " (" + propertyCode + ")");
					} else {
						Toast.makeText(getContext(), getResources().getString(R.string.PropertyDataNotFound), Toast.LENGTH_LONG).show();
						txtProperty.setText(Constants.EMPTY_STRING);
					}
				}
				break;
		}
	}

	private void saveForm() {
		PropertyAssign tempPropertyAssign;

		if (propertyAssign.getId() != null && propertyAssign.getId() > 0) {
			tempPropertyAssign = PropertyAssign.findById(PropertyAssign.class, propertyAssign.getId());

			tempPropertyAssign.SetData(tempPropertyAssign, propertyAssign.getPersonID(), propertyAssign.getPropertyID(), propertyAssign.getLocationID(), txtComment.getText().toString());
		} else {
			tempPropertyAssign = new PropertyAssign(propertyAssign.getPersonID(), propertyAssign.getPropertyID(), propertyAssign.getLocationID(), txtComment.getText().toString());
		}

		tempPropertyAssign.save();
	}

	private void initialForm(View view) {
		txtPerson = (EditText) view.findViewById(R.id.txtPerson);
		txtProperty = (EditText) view.findViewById(R.id.txtProperty);
		txtLocation = (EditText) view.findViewById(R.id.txtLocation);
		txtComment = (EditText) view.findViewById(R.id.txtComment);

		imgPerson = (ImageView) view.findViewById(R.id.imgPerson);
		imgProperty = (ImageView) view.findViewById(R.id.imgProperty);
		imgLocation = (ImageView) view.findViewById(R.id.imgLocation);

		tilPerson = (TextInputLayout) view.findViewById(R.id.tilPerson);
		tilProperty = (TextInputLayout) view.findViewById(R.id.tilProperty);
		tilLocation = (TextInputLayout) view.findViewById(R.id.tilLocation);

		if (propertyAssign == null)
			propertyAssign = new PropertyAssign();

		try {
			txtPerson.setText(Person.findById(Person.class, propertyAssign.getPersonID()).getFullName());
		} catch (Exception ignored) {
		}
		try {
			txtProperty.setText(Property.findById(Property.class, propertyAssign.getPropertyID()).getTitle());
		} catch (Exception ignored) {
		}
		try {
			txtLocation.setText(Location.findById(Location.class, propertyAssign.getLocationID()).getTitle());
		} catch (Exception ignored) {
		}

		txtComment.setText(propertyAssign.getComment());

		txtPerson.requestFocus();
	}

	private boolean validateSave() {
		if (txtPerson.getText().toString().isEmpty()) {
			tilPerson.setError(Helpers.WrapInCustomFont(getContext(), this, getString(R.string.RequiredField)));
			txtPerson.requestFocus();

			return false;
		} else {
			tilPerson.setErrorEnabled(false);
		}

		if (txtProperty.getText().toString().isEmpty()) {
			tilProperty.setError(Helpers.WrapInCustomFont(getContext(), this, getString(R.string.RequiredField)));
			txtProperty.requestFocus();

			return false;
		} else {
			tilProperty.setErrorEnabled(false);
		}

		if (txtLocation.getText().toString().isEmpty()) {
			tilLocation.setError(Helpers.WrapInCustomFont(getContext(), this, getString(R.string.RequiredField)));
			txtLocation.requestFocus();

			return false;
		} else {
			tilLocation.setErrorEnabled(false);
		}

		return true;
	}

	public void SetData(PropertyAssign propertyAssign) {
		PropertyAssignDefineFragment.propertyAssign = propertyAssign;
	}

	public void SetPerson(Person person) {
		propertyAssign.setPersonID(person.getId());
		txtPerson.setText(person.getFullName());
	}

	public void SetLocation(Location location) {
		propertyAssign.setLocationID(location.getId());
		txtLocation.setText(location.getTitle());
	}
}