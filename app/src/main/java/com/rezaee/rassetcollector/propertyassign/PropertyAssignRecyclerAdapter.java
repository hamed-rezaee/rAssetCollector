package com.rezaee.rassetcollector.propertyassign;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orm.SugarRecord;
import com.rezaee.rassetcollector.R;
import com.rezaee.rassetcollector.core.MainActivity;
import com.rezaee.rassetcollector.model.Location;
import com.rezaee.rassetcollector.model.Person;
import com.rezaee.rassetcollector.model.Property;
import com.rezaee.rassetcollector.model.PropertyAssign;

import java.util.ArrayList;
import java.util.List;

public class PropertyAssignRecyclerAdapter extends RecyclerView.Adapter<PropertyAssignRecyclerAdapter.PropertyAssignViewHolder> implements Filterable {
	private Context context;
	private List<PropertyAssign> resource;
	private List<PropertyAssign> filteredResource;
	private LayoutInflater layoutInflater;

	public PropertyAssignRecyclerAdapter(Context context, List<PropertyAssign> propertyAssigns) {
		this.context = context;
		this.resource = propertyAssigns;
		this.filteredResource = propertyAssigns;
		this.layoutInflater = LayoutInflater.from(context);
	}

	@Override
	public PropertyAssignViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = layoutInflater.inflate(R.layout.item_property_assign, parent, false);

		return new PropertyAssignViewHolder(view);
	}

	@Override
	public void onBindViewHolder(PropertyAssignViewHolder holder, int position) {
		holder.SetData(filteredResource.get(position));
	}

	@Override
	public int getItemCount() {
		return filteredResource.size();
	}

	public void deleteItem(final PropertyAssign item, final int currentPosition) {
		new AlertDialog.Builder(context)
			.setTitle(R.string.Caution)
			.setMessage(R.string.AreYouSure)
			.setIcon(R.drawable.ic_caution)
			.setPositiveButton(R.string.Ok, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					if (validateDelete(item)) {
						PropertyAssign.delete(item);

						filteredResource = SugarRecord.listAll(PropertyAssign.class);

						notifyItemRemoved(currentPosition);
					}

					notifyItemRangeChanged(currentPosition, filteredResource.size());
				}
			})
			.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					notifyItemRangeChanged(currentPosition, filteredResource.size());
				}
			})
			.show();
	}

	private boolean validateDelete(PropertyAssign propertyAssign) {
		return true;
	}

	public void editItem(PropertyAssign propertyAssign) {
		PropertyAssignDefineFragment propertyAssignDefineFragment = new PropertyAssignDefineFragment();

		propertyAssignDefineFragment.SetData(propertyAssign);

		FragmentTransaction fragmentTransaction = ((MainActivity) context).getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.frlMainContainer, propertyAssignDefineFragment);
		fragmentTransaction.commit();
	}

	@Override
	public Filter getFilter() {
		return new Filter() {
			@Override
			protected FilterResults performFiltering(CharSequence charSequence) {
				FilterResults results = new FilterResults();

				charSequence = charSequence.toString().toLowerCase();

				if (charSequence.length() == 0) {
					results.values = resource;
					results.count = resource.size();
				} else {
					List<PropertyAssign> filterResultsData = new ArrayList<>();

					for (PropertyAssign data : resource) {
						String personFullName = Person.findById(Person.class, data.getPersonID()).getFullName().toLowerCase();
						String propertyCode = Property.findById(Property.class, data.getPropertyID()).getCode().toLowerCase();
						String propertyTitle = Property.findById(Property.class, data.getPropertyID()).getTitle().toLowerCase();
						String locationTitle = Location.findById(Location.class, data.getLocationID()).getTitle().toLowerCase();

						if (personFullName.contains(charSequence) || propertyCode.contains(charSequence) || propertyTitle.contains(charSequence) || locationTitle.contains(charSequence))
							filterResultsData.add(data);
					}

					results.values = filterResultsData;
					results.count = filterResultsData.size();
				}

				return results;
			}

			@Override
			protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
				filteredResource = (ArrayList<PropertyAssign>) filterResults.values;

				notifyDataSetChanged();
			}
		};
	}

	class PropertyAssignViewHolder extends RecyclerView.ViewHolder {
		public PropertyAssign currentObject;

		private TextView lblPropertyTitle;
		private TextView lblPropertyCode;
		private TextView lblPersonTitle;
		private TextView lblLocationTitle;
		private TextView lblComment;

		public PropertyAssignViewHolder(View itemView) {
			super(itemView);

			lblPropertyTitle = (TextView) itemView.findViewById(R.id.lblPropertyTitle);
			lblPropertyCode = (TextView) itemView.findViewById(R.id.lblPropertyCode);
			lblPersonTitle = (TextView) itemView.findViewById(R.id.lblPersonTitle);
			lblLocationTitle = (TextView) itemView.findViewById(R.id.lblLocationTitle);
			lblComment = (TextView) itemView.findViewById(R.id.lblComment);

			LinearLayout cmdEdit = (LinearLayout) itemView.findViewById(R.id.cmdEdit);
			LinearLayout cmdDelete = (LinearLayout) itemView.findViewById(R.id.cmdDelete);

			cmdEdit.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					editItem(currentObject);
				}
			});

			cmdDelete.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					deleteItem(currentObject, getAdapterPosition());
				}
			});
		}

		public void SetData(PropertyAssign currentObject) {
			this.currentObject = currentObject;

			Property property = Property.findById(Property.class, currentObject.getPropertyID());

			lblPropertyTitle.setText(property.getTitle());
			lblPropertyCode.setText(property.getCode());
			lblPersonTitle.setText(Person.findById(Person.class, currentObject.getPersonID()).getFullName());
			lblLocationTitle.setText(Location.findById(Location.class, currentObject.getLocationID()).getTitle());
			lblComment.setText(currentObject.getComment().isEmpty() ? context.getResources().getString(R.string.NoComment) : currentObject.getComment());
		}
	}
}