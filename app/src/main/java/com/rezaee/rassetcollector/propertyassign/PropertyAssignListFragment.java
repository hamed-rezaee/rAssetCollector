package com.rezaee.rassetcollector.propertyassign;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.rezaee.rassetcollector.R;
import com.rezaee.rassetcollector.core.Constants;
import com.rezaee.rassetcollector.core.Helpers;
import com.rezaee.rassetcollector.core.MainActivity;
import com.rezaee.rassetcollector.model.PropertyAssign;

public class PropertyAssignListFragment extends Fragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		((MainActivity) getActivity()).setActionBarTitle(getContext().getResources().getString(R.string.PropertyAssignList));

		View view = inflater.inflate(R.layout.fragment_recycler_view_list, container, false);

		Helpers.SetFABVisibility(getActivity(), View.VISIBLE);

		FloatingActionButton fabAddItem = (FloatingActionButton) getActivity().findViewById(R.id.fabAddItem);

		fabAddItem.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				try {
					PropertyAssignDefineFragment propertyAssignDefineFragment = new PropertyAssignDefineFragment();

					getActivity().getSupportFragmentManager()
						.beginTransaction()
						.replace(R.id.frlMainContainer, propertyAssignDefineFragment, Constants.DEFINE_PROPERTY_ASSIGN_FRAGMENT)
						.commit();
				} catch (Exception ignored) {
				}
			}
		});

		setUpRecyclerView(view);

		Helpers.ExpandAppBar(getActivity(), true);

		return view;
	}

	private void setUpRecyclerView(View view) {
		RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.rvuMainRecyclerView);
		LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
		final PropertyAssignRecyclerAdapter recyclerAdapter = new PropertyAssignRecyclerAdapter(getContext(), PropertyAssign.listAll(PropertyAssign.class, "id"));

		recyclerView.setAdapter(recyclerAdapter);

		EditText txtSearch = (EditText) view.findViewById(R.id.txtSearch);

		txtSearch.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
			}

			@Override
			public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
			}

			@Override
			public void afterTextChanged(Editable editable) {
				recyclerAdapter.getFilter().filter(editable);
			}
		});

		linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

		recyclerView.setLayoutManager(linearLayoutManager);
		recyclerView.setItemAnimator(new DefaultItemAnimator());
	}
}