package com.rezaee.rassetcollector.person;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.rezaee.rassetcollector.R;
import com.rezaee.rassetcollector.core.Helpers;
import com.rezaee.rassetcollector.core.MainActivity;
import com.rezaee.rassetcollector.model.Person;

public class PersonDefineFragment extends Fragment {
	EditText txtFirstName;
	EditText txtLastName;
	EditText txtPosition;
	EditText txtTel;

	TextInputLayout tilFirstName;
	TextInputLayout tilLastName;

	private Person person;

	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		((MainActivity) getActivity()).setActionBarTitle(getContext().getResources().getString(R.string.Persons));

		View view = inflater.inflate(R.layout.fragment_define_person, container, false);

		initialForm(view);

		Button cmdSave = (Button) view.findViewById(R.id.cmdSave);
		Button cmdCancel = (Button) view.findViewById(R.id.cmdCancel);

		cmdSave.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (validateSave()) {
					saveForm();

					getActivity().getSupportFragmentManager()
						.beginTransaction()
						.replace(R.id.frlMainContainer, new PersonListFragment())
						.commit();

					Helpers.HideKeypad(getActivity(), view);

					((MainActivity) getActivity()).setActionBarTitle(getContext().getResources().getString(R.string.app_name));

					final Snackbar snackbar = Snackbar.make(view, R.string.OperationSuccessfullyCompleted, Snackbar.LENGTH_SHORT);

					snackbar.setAction(R.string.Ok, new View.OnClickListener() {
						@Override
						public void onClick(View view) {
							snackbar.dismiss();
						}
					}).show();
				}
			}
		});

		cmdCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(final View view) {
				getActivity().getSupportFragmentManager()
					.beginTransaction()
					.replace(R.id.frlMainContainer, new PersonListFragment())
					.commit();

				Helpers.HideKeypad(getActivity(), view);
			}
		});

		Helpers.ExpandAppBar(getActivity(), false);
		Helpers.SetFABVisibility(getActivity(), View.GONE);

		return view;
	}

	private void saveForm() {
		Person tempPerson;

		if (person.getId() != null && person.getId() > 0) {
			tempPerson = Person.findById(Person.class, person.getId());

			tempPerson.SetData(tempPerson, txtFirstName.getText().toString(), txtLastName.getText().toString(), txtPosition.getText().toString(), txtTel.getText().toString());
		} else {
			tempPerson = new Person(txtFirstName.getText().toString(), txtLastName.getText().toString(), txtPosition.getText().toString(), txtTel.getText().toString());
		}

		tempPerson.save();
	}

	private void initialForm(View view) {
		txtFirstName = (EditText) view.findViewById(R.id.txtFirstName);
		txtLastName = (EditText) view.findViewById(R.id.txtLastName);
		txtPosition = (EditText) view.findViewById(R.id.txtPosition);
		txtTel = (EditText) view.findViewById(R.id.txtTel);

		tilFirstName = (TextInputLayout) view.findViewById(R.id.tilFirstName);
		tilLastName = (TextInputLayout) view.findViewById(R.id.tilLastName);

		if (person == null)
			person = new Person();

		txtFirstName.setText(person.getFirstName());
		txtLastName.setText(person.getLastName());
		txtPosition.setText(person.getAccountNo());
		txtTel.setText(person.getCardNo());

		txtFirstName.requestFocus();
	}

	private boolean validateSave() {
		if (txtFirstName.getText().toString().isEmpty()) {
			tilFirstName.setError(Helpers.WrapInCustomFont(getContext(), this, getString(R.string.RequiredField)));
			txtFirstName.requestFocus();

			return false;
		} else {
			tilFirstName.setErrorEnabled(false);
		}

		if (txtLastName.getText().toString().isEmpty()) {
			tilLastName.setError(Helpers.WrapInCustomFont(getContext(), this, getString(R.string.RequiredField)));
			txtLastName.requestFocus();

			return false;
		} else {
			tilLastName.setErrorEnabled(false);
		}

		return true;
	}

	public void SetData(Person person) {
		this.person = person;
	}
}