package com.rezaee.rassetcollector.person;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.orm.SugarRecord;
import com.rezaee.rassetcollector.R;
import com.rezaee.rassetcollector.core.MainActivity;
import com.rezaee.rassetcollector.model.Person;
import com.rezaee.rassetcollector.model.PropertyAssign;

import java.util.ArrayList;
import java.util.List;

public class PersonRecyclerAdapter extends RecyclerView.Adapter<PersonRecyclerAdapter.PersonViewHolder> implements Filterable {
	private Context context;
	private List<Person> resource;
	private List<Person> filteredResource;
	private LayoutInflater layoutInflater;

	public PersonRecyclerAdapter(Context context, List<Person> persons) {
		this.context = context;
		this.resource = persons;
		this.filteredResource = persons;
		this.layoutInflater = LayoutInflater.from(context);
	}

	@Override
	public PersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = layoutInflater.inflate(R.layout.item_person, parent, false);

		return new PersonViewHolder(view);
	}

	@Override
	public void onBindViewHolder(PersonViewHolder holder, int position) {
		holder.SetData(filteredResource.get(position));
	}

	@Override
	public int getItemCount() {
		return filteredResource.size();
	}

	public void deleteItem(final Person item, final int currentPosition) {
		new AlertDialog.Builder(context)
			.setTitle(R.string.Caution)
			.setMessage(R.string.AreYouSure)
			.setIcon(R.drawable.ic_caution)
			.setPositiveButton(R.string.Ok, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					if (validateDelete(item)) {
						Person person = Person.findById(Person.class, item.getId());
						person.delete();

						filteredResource = SugarRecord.listAll(Person.class);

						notifyItemRemoved(currentPosition);
					}

					notifyItemRangeChanged(currentPosition, filteredResource.size());
				}
			})
			.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					notifyItemRangeChanged(currentPosition, filteredResource.size());
				}
			})
			.show();
	}

	private boolean validateDelete(Person person) {
		long resultCount = PropertyAssign.count(PropertyAssign.class, "person_id = ?", new String[]{String.valueOf(person.getId())});

		if (resultCount > 0) {
			Toast.makeText(context, R.string.ItemIsInUse, Toast.LENGTH_LONG).show();

			return false;
		}

		return true;
	}

	public void editItem(Person person) {
		PersonDefineFragment personDefineFragment = new PersonDefineFragment();

		personDefineFragment.SetData(person);

		FragmentTransaction fragmentTransaction = ((MainActivity) context).getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.frlMainContainer, personDefineFragment);
		fragmentTransaction.commit();
	}

	@Override
	public Filter getFilter() {
		return new Filter() {
			@Override
			protected FilterResults performFiltering(CharSequence charSequence) {
				FilterResults results = new FilterResults();

				charSequence = charSequence.toString().toLowerCase();

				if (charSequence.length() == 0) {
					results.values = resource;
					results.count = resource.size();
				} else {
					List<Person> filterResultsData = new ArrayList<>();

					for (Person data : resource) {
						String fullName = data.getFullName();

						if (fullName.contains(charSequence))
							filterResultsData.add(data);
					}

					results.values = filterResultsData;
					results.count = filterResultsData.size();
				}

				return results;
			}

			@Override
			protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
				filteredResource = (ArrayList<Person>) filterResults.values;

				notifyDataSetChanged();
			}
		};
	}

	class PersonViewHolder extends RecyclerView.ViewHolder {
		public Person currentObject;

		private TextView lblFullName;

		public PersonViewHolder(View itemView) {
			super(itemView);

			lblFullName = (TextView) itemView.findViewById(R.id.lblFullName);

			LinearLayout cmdEdit = (LinearLayout) itemView.findViewById(R.id.cmdEdit);
			LinearLayout cmdDelete = (LinearLayout) itemView.findViewById(R.id.cmdDelete);

			cmdEdit.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					editItem(currentObject);
				}
			});

			cmdDelete.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					deleteItem(currentObject, getAdapterPosition());
				}
			});
		}

		public void SetData(Person currentObject) {
			this.currentObject = currentObject;

			lblFullName.setText(currentObject.getFullName());
		}
	}
}