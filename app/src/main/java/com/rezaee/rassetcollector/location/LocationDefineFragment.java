package com.rezaee.rassetcollector.location;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.rezaee.rassetcollector.R;
import com.rezaee.rassetcollector.core.Helpers;
import com.rezaee.rassetcollector.core.MainActivity;
import com.rezaee.rassetcollector.model.Location;

public class LocationDefineFragment extends Fragment {
	EditText txtTitle;
	CheckBox chkActive;

	TextInputLayout tilTitle;

	private Location location;

	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		((MainActivity) getActivity()).setActionBarTitle(getContext().getResources().getString(R.string.Locations));

		View view = inflater.inflate(R.layout.fragment_define_location, container, false);

		initialForm(view);

		Button cmdSave = (Button) view.findViewById(R.id.cmdSave);
		Button cmdCancel = (Button) view.findViewById(R.id.cmdCancel);

		cmdSave.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (validateSave()) {
					saveForm();

					getActivity().getSupportFragmentManager()
						.beginTransaction()
						.replace(R.id.frlMainContainer, new LocationListFragment())
						.commit();

					Helpers.HideKeypad(getActivity(), view);

					((MainActivity) getActivity()).setActionBarTitle(getContext().getResources().getString(R.string.app_name));

					final Snackbar snackbar = Snackbar.make(view, R.string.OperationSuccessfullyCompleted, Snackbar.LENGTH_SHORT);

					snackbar.setAction(R.string.Ok, new View.OnClickListener() {
						@Override
						public void onClick(View view) {
							snackbar.dismiss();
						}
					}).show();
				}
			}
		});

		cmdCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(final View view) {
				getActivity().getSupportFragmentManager()
					.beginTransaction()
					.replace(R.id.frlMainContainer, new LocationListFragment())
					.commit();

				Helpers.HideKeypad(getActivity(), view);
			}
		});

		Helpers.ExpandAppBar(getActivity(), false);
		Helpers.SetFABVisibility(getActivity(), View.GONE);

		return view;
	}

	private void saveForm() {
		Location tempLocation;

		if (location.getId() != null && location.getId() > 0) {
			tempLocation = Location.findById(Location.class, location.getId());

			tempLocation.SetData(tempLocation, txtTitle.getText().toString(), chkActive.isChecked());
		} else {
			tempLocation = new Location(txtTitle.getText().toString(), chkActive.isChecked());
		}

		tempLocation.save();
	}

	private void initialForm(View view) {
		txtTitle = (EditText) view.findViewById(R.id.txtTitle);
		chkActive = (CheckBox) view.findViewById(R.id.chkActive);

		tilTitle = (TextInputLayout) view.findViewById(R.id.tilTitle);

		if (location == null)
			location = new Location();

		txtTitle.setText(location.getTitle());
		chkActive.setChecked(location.isActive());
	}

	private boolean validateSave() {
		if (txtTitle.getText().toString().isEmpty()) {
			tilTitle.setError(Helpers.WrapInCustomFont(getContext(), this, getString(R.string.RequiredField)));
			txtTitle.requestFocus();

			return false;
		} else {
			tilTitle.setErrorEnabled(false);
		}

		return true;
	}

	public void SetData(Location location) {
		this.location = location;
	}
}