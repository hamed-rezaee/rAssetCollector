package com.rezaee.rassetcollector.location;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.orm.SugarRecord;
import com.rezaee.rassetcollector.R;
import com.rezaee.rassetcollector.core.MainActivity;
import com.rezaee.rassetcollector.model.Location;
import com.rezaee.rassetcollector.model.PropertyAssign;

import java.util.ArrayList;
import java.util.List;

public class LocationRecyclerAdapter extends RecyclerView.Adapter<LocationRecyclerAdapter.LocationViewHolder> implements Filterable {
	private Context context;
	private List<Location> resource;
	private List<Location> filteredResource;
	private LayoutInflater layoutInflater;

	public LocationRecyclerAdapter(Context context, List<Location> locations) {
		this.context = context;
		this.resource = locations;
		this.filteredResource = locations;
		this.layoutInflater = LayoutInflater.from(context);
	}

	@Override
	public LocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = layoutInflater.inflate(R.layout.item_location, parent, false);

		return new LocationViewHolder(view);
	}

	@Override
	public void onBindViewHolder(LocationViewHolder holder, int position) {
		holder.SetData(filteredResource.get(position));
	}

	@Override
	public int getItemCount() {
		return filteredResource.size();
	}

	public void deleteItem(final Location item, final int currentPosition) {
		new AlertDialog.Builder(context)
			.setTitle(R.string.Caution)
			.setMessage(R.string.AreYouSure)
			.setIcon(R.drawable.ic_caution)
			.setPositiveButton(R.string.Ok, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					if (validateDelete(item)) {
						Location location = Location.findById(Location.class, item.getId());
						location.delete();

						filteredResource = SugarRecord.listAll(Location.class);

						notifyItemRemoved(currentPosition);
					}

					notifyItemRangeChanged(currentPosition, filteredResource.size());
				}
			})
			.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					notifyItemRangeChanged(currentPosition, filteredResource.size());
				}
			})
			.show();
	}

	private boolean validateDelete(Location location) {
		long resultCount = PropertyAssign.count(PropertyAssign.class, "location_id = ?", new String[]{String.valueOf(location.getId())});

		if (resultCount > 0) {
			Toast.makeText(context, R.string.ItemIsInUse, Toast.LENGTH_LONG).show();

			return false;
		}

		return true;
	}

	public void editItem(Location location) {
		LocationDefineFragment locationDefineFragment = new LocationDefineFragment();

		locationDefineFragment.SetData(location);

		FragmentTransaction fragmentTransaction = ((MainActivity) context).getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.frlMainContainer, locationDefineFragment);
		fragmentTransaction.commit();
	}

	@Override
	public Filter getFilter() {
		return new Filter() {
			@Override
			protected FilterResults performFiltering(CharSequence charSequence) {
				FilterResults results = new FilterResults();

				charSequence = charSequence.toString().toLowerCase();

				if (charSequence.length() == 0) {
					results.values = resource;
					results.count = resource.size();
				} else {
					List<Location> filterResultsData = new ArrayList<>();

					for (Location data : resource) {
						String title = data.getTitle();

						if (title.contains(charSequence))
							filterResultsData.add(data);
					}

					results.values = filterResultsData;
					results.count = filterResultsData.size();
				}

				return results;
			}

			@Override
			protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
				filteredResource = (ArrayList<Location>) filterResults.values;

				notifyDataSetChanged();
			}
		};
	}

	class LocationViewHolder extends RecyclerView.ViewHolder {
		public Location currentObject;

		private TextView lblTitle;

		public LocationViewHolder(View itemView) {
			super(itemView);

			lblTitle = (TextView) itemView.findViewById(R.id.lblTitle);

			LinearLayout cmdEdit = (LinearLayout) itemView.findViewById(R.id.cmdEdit);
			LinearLayout cmdDelete = (LinearLayout) itemView.findViewById(R.id.cmdDelete);

			cmdEdit.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					editItem(currentObject);
				}
			});

			cmdDelete.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					deleteItem(currentObject, getAdapterPosition());
				}
			});
		}

		public void SetData(Location currentObject) {
			this.currentObject = currentObject;

			lblTitle.setText(currentObject.getTitle());
		}
	}
}