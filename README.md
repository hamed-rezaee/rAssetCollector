# rAssetCollector (Asset Manager)
An app for managing assets... (Android)

## Dependencies
    Sugar ORM:
        http://satyan.github.io/sugar/
    Calligraphy:
        https://github.com/chrisjenx/Calligraphy
    Sun Date Picker:
        https://github.com/alirezaafkar/SunDatePicker
    MP Android Chart:
        https://github.com/PhilJay/MPAndroidChart
    Swipe Layout Android:
        https://github.com/rambler-digital-solutions/swipe-layout-android
	Barcode Scanner:
        https://github.com/dm77/barcodescanner

## Licence
    Copyright© 2017 Hamed Rezaee
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.